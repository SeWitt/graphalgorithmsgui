package com.kotter.graph;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

public class SquareGraph extends Graph{
    private HashMap<GraphElement, Double> nodeDistances;

    public SquareGraph(int dimension){
        super();
        this.setDimension(dimension * dimension);
        nodeDistances = new HashMap<>(this.getDimension());
        graphMatrix = new ArrayList<ArrayList<GraphElement>>(this.getDimension());
        for(int i = 0; i < this.getDimension(); i++) {
            graphMatrix.add(new ArrayList<GraphElement>(dimension));
            for(int j = 0; j < this.getDimension(); j++) {
                int xI = i % dimension;
                int yI = i / dimension;
                int xJ = j % dimension;
                int yJ = j / dimension;
                Point point = new Point(xI, yI);
                Point point2 = new Point(xJ, yJ);
                Double distance = point.distance(point2);
                graphMatrix.get(i).add(j, new GraphElement(i,j, 1));
                nodeDistances.put(getEdgeAt(i,j), distance);
            }
        }

    }
}
