package com.kotter.graph;

import java.util.ArrayList;

/*** Element for storing weights for edges
 *
 */
class GraphElement {
	protected ArrayList<Integer> values;
	private String name;
	private int firstIncidentNode;
	private int secondIncidentNode;
	
	GraphElement(int sourceNode, int targetNode, Integer value, Integer...rest){
		this.setName(makeNameFromNodes(sourceNode,targetNode));
		this.setIncidentNodesFromName();
		this.values = new ArrayList<Integer>(1 + rest.length);
		values.add(value);
		for(Integer r : rest) {
			values.add(r);
		}

	}

	private String makeNameFromNodes(int sourceNode, int targetNode){
		return sourceNode + ":" + targetNode;
	}

	public Integer getValueAt(int position) {
		return values.get(position);
	}
	

	public Integer getValue() {
		Boolean bool = true;
		return values.get(0);
	}

	/**
	 * Sets value at given position in List for GraphElement values.
	 * @param value
	 * @param position Default is 0
	 */
	public void setValueAt(Integer value, int position) {
		if((int)value >= 0 && position < values.size()) {
			this.values.set(position, value);
		}
		else{
			throw new IllegalArgumentException("Value is not in bounds: value = " + value + " and position = " + position);
		}
	}
	
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return String.format("(%s, %s)", this.getName(), this.getValue());
	}


	private	void setIncidentNodesFromName(){
		ArrayList<Integer> tmpIncidentNodes = new ArrayList<>(2);
		String edgeName = this.getName();
		int indexOfColon = edgeName.indexOf(':');
		String firstNode = edgeName.substring(0,indexOfColon);
		String secondNode = edgeName.substring(indexOfColon + 1);
		if(indexOfColon > 0) {
			firstIncidentNode = Integer.valueOf(firstNode);
			secondIncidentNode = Integer.valueOf(secondNode);
		}
		else{
			throw new IllegalArgumentException("Edge name is ill-formed: " + edgeName);
		}

	}

	public int getSecondIncidentNode() {
		return secondIncidentNode;
	}

	public int getFirstIncidentNode() {
		return firstIncidentNode;
	}
}
