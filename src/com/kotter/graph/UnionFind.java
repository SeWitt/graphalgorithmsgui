package com.kotter.graph;


import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;

class UnionFind extends GraphAlgorithms{

    private int[] parent;
    private int[] rank;
    private int connectedComponentsCount;

    UnionFind(int nodeCount){
        parent = new int[nodeCount];
        rank = new int [nodeCount];
        connectedComponentsCount = nodeCount;
        for(int i = 0; i < nodeCount; i++){
            parent[i] = i;
            rank[0] = 0;
        }
    }

    void Union(int sourceNode, int targetNode){
        if(nodesAreOutOfBounds(sourceNode, targetNode)){
            throw new IllegalArgumentException("One or both Nodes out of Bounds. First Node = " + sourceNode + ", Second Node = " + targetNode);
        }
        int rootOfSourceNode = findRootOf(sourceNode);
        int rootOfTargetNode = findRootOf(targetNode);
        UnionOfRootNodes(rootOfSourceNode, rootOfTargetNode);
    }

    private void UnionOfRootNodes(int rootOfSourceNode, int rootOfTargetNode) {
        if(rootOfSourceNode == rootOfTargetNode){
            return;
        }
        if(rank[rootOfSourceNode] > rank[rootOfTargetNode]){
            parent[rootOfTargetNode] = rootOfSourceNode;
        }
        else {
            parent[rootOfSourceNode] = rootOfTargetNode;
            if(rank[rootOfSourceNode] == rank[rootOfTargetNode]){
                rank[rootOfTargetNode] += 1;
            }
        }
        connectedComponentsCount -= 1;
    }

    private boolean nodesAreOutOfBounds(int sourceNode, int targetNode) {
        return sourceNode < 0 || sourceNode >= parent.length || targetNode < 0 || targetNode >= parent.length;
    }

    int findRootOf(int node){
        if(node >= 0 && node < parent.length){
            int tmpNode = node;
            while (parent[node] != node) {
                node = parent[node];
            }
            parent[tmpNode] = node;
            return node;
        }
        else{
            throw new IllegalArgumentException("Node existiert nicht! Übergebene Nummer: " + node);
        }
    }

    boolean connected(int sourceNode, int targetNode){
        return findRootOf(sourceNode) == findRootOf(targetNode);
    }

    int getConnectedComponentsCount() {
        return connectedComponentsCount;
    }
    void printUnionFind(){
        String a = "";
        for(int i = 0; i < parent.length; i++){
            a = a + i + ":" + parent[i] + " ";
        }
        System.out.println(a);
    }
}
