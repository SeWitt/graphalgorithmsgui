package com.kotter.graph;

import java.util.Stack;

public abstract class GraphAlgorithms {
	static public int depthFirstSearch(Graph graph, int target) {
		int dimension = graph.getDimension();
		Boolean[] isVisited = new Boolean[dimension];
		for(int i = 0; i < dimension; i++) {
			isVisited[i] = false;
		}
		int outerLoopCount = 0;
		Stack<Integer> callStack = new Stack<>();
		callStack.push(0);
		while(!callStack.isEmpty()) {
			int pop = callStack.pop();
			System.out.print(pop + "-> ");
			isVisited[pop] = true;
			if(pop == target) return pop;
			for(int i = 0; i < dimension; i++) {
				if(graph.graphMatrix.get(pop).get(i).getValue() == 1 && !isVisited[i]) {
					callStack.push(i);
				}
				else if(isVisited[i]) {
					//System.out.println("Kreis gefunden! Von " + pop + " und " + i);
					
				}
			}
			for(int i = outerLoopCount++; i < dimension; i++) {
				if(!isVisited[i]) {
					callStack.push(i);
				}
			}
		}
		return -1;
	}

}
