package com.kotter.graph;

import graphGUI.graphGUI;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EngineRoom {
	static Logger logger = LogManager.getLogger();

	public static void main(String[] args) {
		logger.info("Starte Programm");

		// Create Random Graph with the given model
		RandomGraph graph = new RandomGraph(10);
//		SquareGraph sGraph = new SquareGraph(4);
//		sGraph.printGraph();

		//TODO setRandomEdgeWeights should be inside the graph Package.
		// Sets random weights to each edge. Weights are Integer in graph.setRandomEdgeWeights(); the interval [0,100](?)

		graphGUI tg = new graphGUI();
		graphGUI.launch(tg.getClass());
	}
}
