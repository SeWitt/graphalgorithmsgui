package com.kotter.graph;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Random;

public class Graph {
	static Logger logger = LogManager.getLogger();

	private int dimension = 0;
	int edgeCount = 0;

	ArrayList<ArrayList<GraphElement>> graphMatrix;
	
	/**
	 * Create empty CoreGraph
	 */
	public Graph(int dimension){
		graphMatrix = new ArrayList<ArrayList<GraphElement>>(dimension);
		for(int i = 0; i < dimension; i++) {
			graphMatrix.add(new ArrayList<GraphElement>(dimension));
			for(int j = 0; j < dimension; j++) {
				graphMatrix.get(i).add(j, new GraphElement(i,j, 0));
			}
		}
		this.dimension = dimension;
	}

	public Graph(Boolean[][] network){
		this(network.length);
		for(int i = 0; i < this.dimension; i++) {
			for(int j = 0; j < this.dimension; j++) {
				if(network[i][j] != null && network[i][j]) {
					this.graphMatrix.get(i).get(j).setValueAt(1, 0);
				}
				else {
					this.graphMatrix.get(i).get(j).setValueAt(0, 0);
				}
				
			}
		}
	}

	public Graph() {
	}

	public Graph(Graph graph){
		this.dimension = graph.getDimension();
		graphMatrix = new ArrayList<ArrayList<GraphElement>>(dimension);
		for(int i = 0; i < dimension; i++) {
			graphMatrix.add(new ArrayList<GraphElement>(dimension));
			for(int j = 0; j < dimension; j++) {
				graphMatrix.get(i).add(j, new GraphElement(i,j, graph.getEdgeAt(i,j).getValue()));
			}
		}
	}

	protected void clearGraph(){
		if(dimension > 0){
			for(int i = 0; i < dimension; i++){
				for(int j = 0; j < dimension; j++){
					getEdgeAt(i, j).setValueAt(1,0);
				}
			}
			edgeCount = 0;
		}
		else{
			throw new IllegalStateException("Cannot clear non existing Graph. Dimension of graphMatrix is: " + dimension);
		}
	}

	void setDimension(int dimension) {
		this.dimension = dimension;
	}

	public int getDimension() {
		return dimension;
	}

	protected GraphElement getEdgeAt(int row, int column){
			return graphMatrix.get(row).get(column);
	}

	protected void setEdge(@NotNull GraphElement edge){
		this.graphMatrix.get(edge.getFirstIncidentNode()).set(edge.getSecondIncidentNode(), edge);
	}

	protected void setEdgeAt(int row, int column, int value) {
		if(row < dimension && column < dimension && value >= 0) {
			edgeCount++;
			graphMatrix.get(row).get(column).setValueAt(value, 0);
		}
		else{
			throw new IllegalArgumentException("Parameter out of bounds");
		}
	}


	public boolean isEdgePresentBetween(int sourceNode, int targetNode) {

		return getEdgeAt(sourceNode, targetNode).getValue() > 0;
	}

	
	/*** Sets random Weights to existing edges by randomNumberGenerator.
	 * Set to integer values between 0 (inclusive) and 100 (exclusive)
	 */
	public void setRandomEdgeWeights() {
		Random randomNumberGenerator = new Random();
		for(ArrayList<GraphElement> e: this.graphMatrix) {
			for(GraphElement ge: e) {
				if(ge.getValue() > 0) {
					ge.setValueAt(randomNumberGenerator.nextInt(100), 0);
				}
			}
		}
	}

	/**
	 * For debug purpose only. TODO to be deleted!
	 */
	public void printGraph() {
		for(int i = 0; i < dimension; i++) {
			System.out.print(i + "\t: ");
			for(int j = 0; j < dimension; j++) {
				System.out.print(graphMatrix.get(i).get(j).getValue() + " ");
			}
			System.out.println();
		}
	}
}

