package com.kotter.graph;

import java.util.Stack;

public class MinimumSpanningTree extends Graph {
    private final UnionFind unionFind;
    private final Graph MST;
    private final Heap minHeap;

    public MinimumSpanningTree(Graph graph){
        this.unionFind = new UnionFind(graph.getDimension());
        this.minHeap = new Heap(graph);
        this.MST = new Graph(graph.getDimension());
        createMinimalSpanningTree();
    }

    /**
     * Kruskal's Algorithm for Minimum Spanning Tree
     */
    public void createMinimalSpanningTree() {
        for (int i = 0; i < minHeap.heap.size(); i++) {
            GraphElement edge = minHeap.deleteMin();
            if (!isConnected(edge.getFirstIncidentNode(),edge.getSecondIncidentNode())) {
                unionFind.Union(edge.getFirstIncidentNode(),edge.getSecondIncidentNode());
                MST.setEdge(edge);
            }
        }
    }

    public Graph getMST(){
        return new Graph(this.MST);
    }

    private boolean isConnected(int firstIncidentNode, int secondIncidentNode) {
        return this.unionFind.connected(firstIncidentNode,secondIncidentNode);
    }
    public static void checkForCyclese(Graph graph){
        boolean[] nodeVisited = new boolean[graph.getDimension()];
        for(int i = 0; i < graph.getDimension(); i++){
            nodeVisited[i] = false;
        }
        Stack<Integer> callStack = new Stack<>();
        for(int i = 0; i < graph.getDimension(); i++){
            if(nodeVisited[i]){
                continue;
            }
            callStack.push(i);
            nodeVisited[i] = true;
            while(!callStack.empty()){
                Integer topOfStack = callStack.pop();
                for(int j = 0; j < graph.getDimension(); j++){
                    if(graph.isEdgePresentBetween(topOfStack, j)){
                        if(nodeVisited[j] && j != topOfStack && !graph.isEdgePresentBetween(topOfStack, j)){
                            System.out.println("Kreis gefunden zwischen i = " + i + " und j = " + topOfStack);
                        }
                        nodeVisited[j] = true;
                        callStack.push(j);
                    }
                }
            }
        }
    }
}