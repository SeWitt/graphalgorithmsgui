package com.kotter.graph;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.provider.*;

import static org.junit.jupiter.api.Assertions.*;

class MinimumSpanningTreeTest {

    @ParameterizedTest
    @ValueSource(ints = {1,2,3,4,5,6,7})
    void createMinimalSpanningTree(int arg) {
        System.out.println(arg);
        assertTrue(true);
    }
    @ParameterizedTest
    @DisplayName("Hallo!")
    @NullAndEmptySource
    @ValueSource(strings = { " ", "   ", "\t", "\n" })
    @CsvSource({
            " ",
            " "
    })
    void nullEmptyAndBlankStrings(String text) {
        assertTrue(text == null || text.trim().isEmpty());
    }

}