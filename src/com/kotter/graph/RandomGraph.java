package com.kotter.graph;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

public class RandomGraph extends Graph {
    static Logger logger = LogManager.getLogger();

    public RandomGraph(int dimension) {
        super(dimension);
    }

    static public Boolean[][] createGNM(int n, int m) {
        //TODO implement me!
        throw new RuntimeException("Function not implemented!");
    }

    /**
     * Creates a random graph in the gnp model, with n nodes and probability p for every (possible) edge to be part of the graph
     * @param p Probability for each Edge to be in the graph
     * @return void
     */
    public void createGNP(double p) {
        int n = getDimension();
        logger.info("gnp used! with n= " + n + " and p= " + p);

        // check if Graph is empty, otherwise random edges would be on top of already existing edges...
        if (edgeCount == 0) {
            if (p <= 1.0 && p > 0 && n >= 0) {
                computeGNP(n, p);
            } else {
                throw new IllegalArgumentException("p must be between 1 and 0, was: " + p + ". n must be non-negative, was: " + n);
            }
        }
        // clear graph of all existing edges
        else {
            this.clearGraph();
            computeGNP(n, p);
        }
    }

    private void computeGNP(int n, double p) {
        int v = 1; //Zeile
        int w = -1; //Spalte
        while (v < n) {
            double r = Math.random();
            w = w + 1 + (int) Math.floor(Math.log(1 - r) / Math.log(1 - p));
            while (w >= v && v < n) {
                w = w - v;
                v += 1;
            }
            if (v < n) {
                edgeCount++;
                this.getEdgeAt(v, w).setValueAt(1, 0);
            }
        }
    }

    /**
     * Create a random graph after the PreferentialAttachment model
     * @param degree minimum degree of a node. can be less then that in graphGUI because selfloops are not allowed there
     */
    public void createPreferentialAttachment(int degree) {
        System.out.println("pref att used! mit n= " + getDimension() + " and degree= " + degree);
        Integer[] tmp = new Integer[2 * getDimension() * degree];

        if (edgeCount == 0) {
            tmp = computePrefAtt(degree);
        } else {
            clearGraph();
            tmp = computePrefAtt(degree);
        }

        // Fill graphMatrix with computet edges
        fillGraphMatrixWithPrefAtt(degree, tmp);
    }

    private void fillGraphMatrixWithPrefAtt(int degree, Integer[] tmp) {
        for (int i = 0; i < getDimension() * degree - 1; i++) {
            edgeCount++;
            int edgeFrom = tmp[2 * i];
            int edgeTo = tmp[2 * i + 1];
            this.getEdgeAt(edgeFrom, edgeTo).setValueAt(1, 0);
        }
    }

    private Integer[] computePrefAtt(int degree) {
        //TODO fix Variable Name
        int n = getDimension();
        Integer[] tmp = new Integer[2 * n * degree];
        Random rG = new Random();
        for (int v = 0; v < n; v++) {
            for (int i = 0; i < degree; i++) {
                tmp[2 * (v * degree + i)] = v;
                int tmpInt = 2 * (v * degree + i);
                int r;
                if (tmpInt == 0) {
                    r = 0;
                } else {
                    r = rG.nextInt(tmpInt);
                }
                tmp[2 * (v * degree + i) + 1] = tmp[r];
            }
        }
        return tmp;
    }

    public void setGraphComplete(){
        Random rng = new Random();
        for(int i = 0; i < getDimension(); i++){
            for(int j = 0; j < getDimension(); j++){
                this.setEdgeAt(i,j, rng.nextInt(99) + 1);
            }
        }
    }
}
