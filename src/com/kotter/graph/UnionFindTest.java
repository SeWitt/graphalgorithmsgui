package com.kotter.graph;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;

import static org.junit.jupiter.api.Assertions.*;

class UnionFindTest {

    private int graphSize = 10;
    private UnionFind uFind = new UnionFind(graphSize);

// Testfall
//    UnionFind uf = new UnionFind(10);
//		uf.Union(1,2);
//		uf.Union(1,3);
//		uf.Union(0,4);
//		uf.Union(0,8);
//		uf.Union(4,6);
//		uf.Union(4,5);
//		uf.Union(5,7);
//		uf.printUnionFind();

    @BeforeAll
    static void setupTestOBject(){
        System.out.println("Hallo BeforeAll!");
    }

    @DisplayName("Union Test")
    @org.junit.jupiter.api.Test
    void union() {
    }

    @DisplayName("Find Root")
    @org.junit.jupiter.api.Test
    void findRoot() {
        assertAll(()->{
            assertNotEquals(1, uFind.findRootOf(0));
            assertEquals(1, uFind.findRootOf(1));
            assertEquals(graphSize - 1, uFind.findRootOf(graphSize - 1));
        });

    }

    @DisplayName("FindRootsException")
    @org.junit.jupiter.api.Test()
    void findRootException(){
        UnionFind uFind = new UnionFind(10);
        assertAll(()-> {
                    assertThrows(IllegalArgumentException.class, () ->
                            uFind.findRootOf(11),"Zahl größer");
                    assertThrows(IllegalArgumentException.class, () ->
                            uFind.findRootOf(-1));
        });

    }

    @org.junit.jupiter.api.Test
    void connected() {
        assertAll(()->{
            assertTrue(uFind.connected(1,1));
            assertFalse(uFind.connected(1,0));
            assertThrows(IllegalArgumentException.class, ()-> assertFalse(uFind.connected(1,-1)));
        });

    }
}