package com.kotter.graph;

import java.util.ArrayList;
import java.util.HashMap;


/** Min-Heap for integer weights
 * @author Sebastian Witt
 *
 */
public class Heap {
	protected ArrayList<GraphElement> heap;

	// Key: Name of GraphElement, Value: Value of GraphElement
	private HashMap<String, Integer> nodeHeapLocation;

	private int heapSize;

	Heap(Graph graph){
		this.heapSize = 0;
		this.heap = new ArrayList<GraphElement>(graph.getDimension());
		this.nodeHeapLocation = new HashMap<String, Integer>(graph.getDimension());
		for(int i = 0; i < graph.getDimension(); i++){
			for(int j = 0; j <= i; j++){
				GraphElement node = graph.getEdgeAt(i,j);
				if((int)node.getValue() > 0){
					insertNode(node);
				}
			}
		}
		this.repairDown(0);
	}

	/*** Build a heap from List of GraphElements
	 *
	 */
	Heap(ArrayList<GraphElement> list){
		//TODO Check of List is already a Heap, if @code{true} set @code{nodeHeapLocation} correct

		this.heapSize = 0;
		this.heap = new ArrayList<GraphElement>(list.size());
		nodeHeapLocation = new HashMap<String, Integer>(list.size());

		for(int i = 0; i < list.size(); i++) {
			nodeHeapLocation.put(list.get(i).getName(), i);
			this.insertNode(list.get(i));
		}
		this.repairDown(0);
	}

	/**
	 * Build an empty Heap
	 * @param graphSize
	 */
	Heap(int graphSize){

		this.heapSize = 0;
		this.nodeHeapLocation = new HashMap<String, Integer>(graphSize);
		this.heap = new ArrayList<GraphElement>(graphSize);
	}

	private boolean isHeap(GraphElement list){
		//TODO implement me!
		return false;
	}

	/** Insert Node at the end of the Heap and repair up
	 * @param node The GraphElement<> to be inserted
	 */
	void insertNode(GraphElement node){
		heap.add(node);
		nodeHeapLocation.put(node.getName(), heapSize);
		this.heapSize++;
		this.repairUp(heapSize - 1);
	}
	
	
	GraphElement deleteMin() {
		if(heapSize > 0) {
			GraphElement minGraphElement = this.heap.get(0);
			int tmpLast = this.heapSize;
			this.heap.set(0, this.heap.get(tmpLast - 1));
			nodeHeapLocation.put(this.heap.get(tmpLast - 1).getName(), 0); //set(0, Integer.valueOf(this.heap.get(tmpLast - 1).getName()));
			heapSize--;
			this.repairDown(0);
			return minGraphElement;
		}
		else {
			return new GraphElement(0,0, 0);
		}
	}
	
//	// TODO: fix Method
//	void changePriority(int nodeLocation, int priority){
//		if(nodeLocation < heapSize && priority >= 0) {
//			this.heap.set(nodeLocation, priority);
//			this.repairDown(nodeLocation);
//		}
//	}
//	
//	// TODO: fix Method
//	void removeNode(int nodeLocation) {
//		this.heap.set(nodeLocation, heap.get(heapSize - 1));
//		heapSize--;
//		this.repairDown(nodeLocation);
//	}
	
	
	void buildHeap(ArrayList<Integer> list) {
		
	}
	
	
	void repairUp(int nodeLocation) {
		GraphElement tmpValue = this.heap.get(nodeLocation);
//		System.out.println("Start: Repair Up! An Position" + nodeLocation + " und Wert: " + tmpValue);
		while(nodeLocation > 0 && nodeLocation < this.heapSize) {
			if(tmpValue.getValue() < this.heap.get(nodeLocation/2).getValue()) {
//				System.out.println("Loop: Vgl: " + nodeLocation + " mit " + nodeLocation/2);
				this.heap.set(nodeLocation, this.heap.get(nodeLocation/2));
				this.nodeHeapLocation.put(this.heap.get(nodeLocation/2).getName(), nodeLocation);
				nodeLocation = nodeLocation/2;
			}
			else {
				break;
			}
		}
		this.heap.set(nodeLocation, tmpValue);
		this.nodeHeapLocation.put(tmpValue.getName(), nodeLocation);
//		System.out.println("Ende: Setze an Stelle " + nodeLocation + " Wert " + tmpValue);
	}
	
	
	void repairDown(int nodeLocation) {
//		System.out.println("Start: Repair Down!");
		GraphElement tmpValue = this.heap.get(nodeLocation);
//		System.out.println("Gespeichertert Wert: " + tmpValue + " an Stelle " + nodeLocation);
		while(nodeLocation < heapSize/2) {
			int child = nodeLocation * 2;
			if(child < heapSize && (this.heap.get(child).getValue() > this.heap.get(child + 1).getValue())) {
				child++;
			}
			if(tmpValue.getValue() <= this.heap.get(child).getValue()) {
				break;
			}
				this.heap.set(nodeLocation, this.heap.get(child));
				nodeLocation = child;
		}
		this.heap.set(nodeLocation, tmpValue);
//		System.out.println("nodeLocation: Danach! " + nodeLocation);
	}
	
	
	static ArrayList<GraphElement> heapsort(ArrayList<GraphElement> List){
		Heap heap = new Heap(List);
		ArrayList<GraphElement> sortedList = new ArrayList<>();
		int tmpHeapSize = heap.heapSize;
		for(int i = 0; i < tmpHeapSize; i++) {
			sortedList.add(heap.deleteMin());
		}
		return sortedList;
	}
}
