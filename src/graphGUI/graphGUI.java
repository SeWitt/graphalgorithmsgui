package graphGUI;

import com.kotter.graph.Graph;
import com.kotter.graph.MinimumSpanningTree;
import com.kotter.graph.RandomGraph;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.ArrayList;

public class graphGUI extends Application {
    Text stageHeight;
    Text stageWidth;

    Text infoPaneHeight;
    Text infoPaneWidth;

    Text graphPaneHeight;
    Text graphPaneWidth;

    private RandomGraph graph;
    private Graph outputGraph;

    GridPane graphPane;
    GridPane infoPane;
    VBox controlElements;

    private double probability = 0.5;
    private int randomGraphModel = 0;
    private int amountOfNodes = 10;
    private int amountOfEdges = 10;
    private int minDegree = 2;

    int radius = 100;
    int translateX = 0;
    int translateY = 0;

    TextField amountEdgesTextField;
    TextField degreeTextField;
    TextField amountOfNodesTextField;
    TextField probTextField;

    private int getDegreeTextField() {
        return Integer.valueOf(degreeTextField.getText());
    }

    private void setDegreeTextField() {
        this.amountOfNodesTextField.setText(String.valueOf(this.getNoOfCircles()));
        this.degreeTextField.setText(String.valueOf(this.getMinDegree()));
    }

    private int getAmountEdgesTextField() {
        return Integer.valueOf(amountEdgesTextField.getText());
    }

    private void setAmountEdgesTextField() {
        this.amountEdgesTextField.setText(String.valueOf(this.getAmountOfEdges()));
    }

    private double getProbability() {
        return probability;
    }

    private void setProbability(double probability) {
        if (probability <= 1 && probability >= 0) {
            this.probability = probability;
        } else {
            this.probability = 0;
        }
    }

    /**
     * 0 = gnp (default)
     * 1 = pref attachment
     * 2 = gnm
     * 3 = Complete Graph
     */
    private void setRandomGraphModel(int i) {
        if (i < 4 && i > 0) {
            this.randomGraphModel = i;
        } else {
            this.randomGraphModel = 0;
        }
    }

    private int getRandomGraphModel() {
        return this.randomGraphModel;
    }

    private int getNoOfCircles() {
        return amountOfNodes;
    }

    private void setNoOfCircles(int noOfCircles) {
        if (noOfCircles >= 0) {
            this.amountOfNodes = noOfCircles;
        } else {
            this.amountOfNodes = 0;
        }

    }

    private int getAmountOfEdges() {
        return amountOfEdges;
    }

    private void setAmountOfEdges(int amountOfEdges) {
        this.amountOfEdges = amountOfEdges;
    }

    private int getMinDegree() {
        return minDegree;
    }

    private void setMinDegree(int minDegree) {
        if (minDegree >= 0) {
            this.minDegree = minDegree;
        } else {
            this.minDegree = 0;
        }
    }

    private double getProbTextField() {
        return Double.valueOf(probTextField.getText());
    }

    private void setProbTextField() {
        this.probTextField.setText(String.valueOf(this.getProbability()));
    }

    private int getAmountOfNodesTextField() {
        return Integer.valueOf(amountOfNodesTextField.getText());
    }

    private void setAmountOfNodesTextField() {
        this.amountOfNodesTextField.setText(String.valueOf(this.getNoOfCircles()));
    }

    private Pane createGraphPane() {
        amountOfNodes = graph.getDimension();
        double step = 2 * Math.PI / amountOfNodes;
        ArrayList<Group> textDump = new ArrayList<>(amountOfNodes);
        for (int count = 0; count < amountOfNodes; count++) {
            Circle tmpCircle = new Circle(10d, Color.RED);
            Text text = new Text(String.valueOf(count));
            text.setTranslateX(-5);
            text.setTranslateY(4);

            Group nodeObject = new Group();

            nodeObject.getChildren().addAll(tmpCircle, text);
            nodeObject.setTranslateX((radius * Math.cos(count * step)) + translateX);
            nodeObject.setTranslateY((radius * Math.sin(count * step)) + translateY);
            textDump.add(nodeObject);
        }

        ArrayList<Line> lineDump = new ArrayList<>();
        for (int i = 0; i < amountOfNodes; i++) {
            for (int j = 0; j < amountOfNodes; j++) {
                if (graph.isEdgePresentBetween(i, j)) {
                    Line line = new Line(
                            (radius * Math.cos(i * step)) + translateX,
                            (radius * Math.sin(i * step)) + translateY,
                            (radius * Math.cos(j * step)) + translateX,
                            (radius * Math.sin(j * step)) + translateY);
                    if (outputGraph.isEdgePresentBetween(i, j)) {
                        line.setStroke(Color.BLUE);
                    }

                    lineDump.add(line);
                } else {
//					System.out.println("Edge NOT present!");
                }

            }
        }

        // Put the drawn Lines and Circles into a group
        Pane graphGroup = new Pane();
        graphGroup.getChildren().addAll(lineDump);
        graphGroup.getChildren().addAll(textDump);
        graphGroup.setTranslateX(this.radius);
        graphGroup.setTranslateY(this.radius);
        return graphGroup;
    }

    private void update() {
        infoPane.getChildren().clear();
        infoPane.add(stageHeight, 3, 0);
        infoPane.add(stageWidth, 3, 1);
        infoPane.add(infoPaneHeight, 4, 0);
        infoPane.add(infoPaneWidth, 4, 1);
        infoPane.add(graphPaneHeight, 5, 0);
        infoPane.add(graphPaneWidth, 5, 1);

        infoPaneHeight.setText(String.valueOf(infoPane.getHeight()));
        infoPaneWidth.setText(String.valueOf(infoPane.getWidth()));
        graphPaneHeight.setText(String.valueOf(graphPane.getHeight()));
        graphPaneWidth.setText(String.valueOf(graphPane.getWidth()));

        infoPane.add(new Text(String.valueOf(this.getRandomGraphModel())), 1, 0);
        infoPane.add(new Text("Random Graph Modell:"), 0, 0);
        infoPane.add(new Text("Anzahl Knoten:"), 0, 1);
        infoPane.add(new Text(String.valueOf(this.getNoOfCircles())), 1, 1);

        this.setAmountOfNodesTextField();
        this.setProbTextField();
        this.setAmountOfNodesTextField();
        this.setDegreeTextField();

        this.radius = (int) graphPane.getHeight() * 2 / 5;
        graphPane.getChildren().clear();
        graphPane.getChildren().addAll(createGraphPane());
        graphPane.setClip(new Rectangle(graphPane.getWidth(), graphPane.getHeight()));
    }

    /**
     * Executed before Start(). Used for Initialization, like the member {@code coreGraph} and the Input Textfields.
     */
    @Override
    public void init() {
        this.graph = new RandomGraph(getNoOfCircles());
        this.outputGraph = new Graph(getNoOfCircles());
        graph.createGNP(this.probability);

        this.amountOfNodesTextField = new TextField();
        // Event Handler for amountOfNodesTextField
        EventHandler<KeyEvent> inputHandler = new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                setNoOfCircles(getAmountOfNodesTextField());
            }
        };
        this.amountOfNodesTextField.setOnKeyReleased(inputHandler);

        this.probTextField = new TextField();
        // Event Handler for ProbTextField
        EventHandler<KeyEvent> probInputHandler = new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                setProbability(getProbTextField());
            }
        };
        this.probTextField.setOnKeyReleased(probInputHandler);

        this.degreeTextField = new TextField();
        // EventHandler for degreeTextField
        EventHandler<KeyEvent> degreeInputHandler = new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                setMinDegree(getDegreeTextField());
            }
        };
        this.degreeTextField.setOnKeyReleased(degreeInputHandler);

        this.amountEdgesTextField = new TextField();
        //
        EventHandler<KeyEvent> amountEdgeInputHandler = new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                setAmountOfEdges(getAmountEdgesTextField());
            }
        };
        this.amountEdgesTextField.setOnKeyReleased(amountEdgeInputHandler);

        // Set the initial Values for the TextFields created
        this.setAmountOfNodesTextField();
        this.setProbTextField();
        this.setAmountEdgesTextField();
        this.setDegreeTextField();
    }

    private void shuffleGraph() {
        this.outputGraph = new Graph(getNoOfCircles());
        this.graph = new RandomGraph(getNoOfCircles());
        switch (this.randomGraphModel) {
            case 0:
                graph.createGNP(this.probability);
                break;
            case 1:
                graph.createPreferentialAttachment(minDegree);
                break;
            case 2:
                break;
            case 3:
                graph.setGraphComplete();
                break;
        }
        graphPane.getChildren().clear();
        graphPane.getChildren().setAll(createGraphPane());
    }

    public void debugPrint() {
        graph.printGraph();
        for (int i = 0; i < getAmountOfNodesTextField(); i++) {
            for (int j = i; j < getAmountOfNodesTextField(); j++) {
                System.out.println(graph.isEdgePresentBetween(i, j));
            }
        }
    }

    @Override
    public void start(Stage stage) {
        Border brdr = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT));

        VBox vbox = createMainWindow();

        Scene scene = new Scene(vbox, 640, 300);

        setStage(stage, scene);

        // Top part of vbox is again divided by this HBox
        HBox hbox = createHBox(stage);

        // The left side of hbox is a VBox, for control Elements
        VBox controlElements = createControlElements(brdr);

        // Create Dropdown Menu for chosing the random graph model with the corresponding Event Handler
        ChoiceBox<String> locationChoiceBox = createChoiceBox();

        // Create "Shuffle Graph" Button with the corresponding Event Handler
        Button btn1 = createShuffleGraphButton();
        Button startGraphAlgorithm = createGraphAlgorithmButton();

        // GridPane for arranging TextField Objects
        GridPane leftGridPane = createLeftPane();

        // Add Dropdown Menu, Shuffle Button and GridPane to controlElements
        addItemsToControlElements(controlElements, locationChoiceBox, btn1, leftGridPane, startGraphAlgorithm);

        // Construct new Graph Object
        Pane graphGroup = createGraphPane();

        // Right side of the upper part of the main VBox, is where the Graph is drawn
        createGraphUnderPane(stage, brdr, graphGroup);

        setSizeEventListenerToStage(stage, controlElements);

        // controlElements and pane for drawing the graph are added to the upper part of vbox
        addUpperVBox(hbox, controlElements);

        // Lower Part of main VBox for Infos on the Graph
        GridPane tabPane = createLowerVBox(stage, brdr);

        // Create TextField to display Graph Properties on lower part of VBox
        createInfoTextFields();

        // Add everything to the Main VBox and the VBox to root.
        addLowerVBox(vbox, hbox, tabPane);

        //Displaying the contents of the stage
        stage.show();
    }

    private Button createGraphAlgorithmButton() {
        Button btn = new Button("Use Algorithm");
        EventHandler<MouseEvent> eventHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                useGraphAlgorithm();
                update();
            }
        };
        btn.addEventFilter(MouseEvent.MOUSE_CLICKED, eventHandler);
        return btn;
    }

    private void useGraphAlgorithm() {
        outputGraph = (new MinimumSpanningTree(graph)).getMST();
    }

    private void setStage(Stage stage, Scene scene) {
        //Setting title to the Stage
        stage.setTitle("Ein Graph");

        stage.setMinHeight(339);
        stage.setMinWidth(656);
        //Adding scene to the stage
        stage.setScene(scene);
        // EventHandler for maximizing the Window
        stage.maximizedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                graphPane.prefWidthProperty().set(stage.widthProperty().get() - controlElements.getWidth() - 1);
                update();

            }
        });
    }


    private void addLowerVBox(VBox vbox, HBox hbox, GridPane tabPane) {
        vbox.getChildren().addAll(hbox, tabPane);
    }

    private void createInfoTextFields() {
        stageHeight = new Text();
        stageWidth = new Text();

        infoPaneHeight = new Text();
        infoPaneWidth = new Text();

        graphPaneHeight = new Text();
        graphPaneWidth = new Text();
    }

    private GridPane createLowerVBox(Stage stage, Border brdr) {
        GridPane tabPane = new GridPane();
        tabPane.setBorder(brdr);
        tabPane.setPrefHeight(100);
        tabPane.prefWidthProperty().bind(stage.widthProperty());
        tabPane.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, CornerRadii.EMPTY, null)));
        tabPane.setHgap(5);
        this.infoPane = tabPane;
        return tabPane;
    }

    private void addUpperVBox(HBox hbox, VBox controlElements) {
        hbox.getChildren().addAll(controlElements, graphPane);
    }

    private void setSizeEventListenerToStage(Stage stage, VBox controlElements) {
        stage.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneHeight, Number newSceneHeight) {
                update();
            }
        });

        stage.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneHeight, Number newSceneHeight) {
                graphPane.prefWidthProperty().set(stage.widthProperty().get() - controlElements.getWidth() - 1);
//				graphPane.setClip(new Rectangle(graphPane.getWidth(), graphPane.getHeight()));
                update();
            }
        });
    }

    private void createGraphUnderPane(Stage stage, Border brdr, Pane graphGroup) {
        graphPane = new GridPane();
        graphPane.getChildren().add(graphGroup);
        graphPane.setAlignment(Pos.CENTER);
        graphPane.setBorder(brdr);
        graphPane.setPrefHeight(265);
        graphPane.setMinHeight(265);
        graphPane.setMinWidth(500);
        graphPane.prefWidthProperty().set(stage.widthProperty().get() - 150 - 1);
        graphPane.setBackground(new Background(new BackgroundFill(Color.DARKGRAY, CornerRadii.EMPTY, Insets.EMPTY)));
        graphPane.setClip(new Rectangle(450, 200));
    }

    private void addItemsToControlElements(VBox controlElements, ChoiceBox<String> locationChoiceBox, Button btn1, GridPane leftGridPane, Button startGraphAlgorithm) {
        locationChoiceBox.getSelectionModel().select(0);
        controlElements.getChildren().addAll(locationChoiceBox, leftGridPane, btn1, startGraphAlgorithm);
    }

    private ChoiceBox<String> createChoiceBox() {
        ChoiceBox<String> locationChoiceBox = new ChoiceBox<>();
        locationChoiceBox.getItems().addAll("G(n,p)", "Preferential Attachment", "G(n,m)", "Complete Graph");
        locationChoiceBox.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldValue, Number newValue) {
                setRandomGraphModel((int) newValue);
            }
        });
        return locationChoiceBox;
    }

    private Button createShuffleGraphButton() {
        Button btn1 = new Button("Shuffle Graph");
        EventHandler<MouseEvent> eventHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                shuffleGraph();
                update();
            }
        };
        btn1.addEventFilter(MouseEvent.MOUSE_CLICKED, eventHandler);
        return btn1;
    }

    private VBox createControlElements(Border brdr) {
        controlElements = new VBox(10);
        controlElements.setAlignment(Pos.TOP_CENTER);
        controlElements.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, CornerRadii.EMPTY, null)));
        controlElements.setBorder(brdr);
        controlElements.setMinWidth(150);
        controlElements.setPrefHeight(200);
        controlElements.setPrefWidth(150);
        return controlElements;
    }

    private HBox createHBox(Stage stage) {
        HBox hbox = new HBox();
        hbox.setMinHeight(200);
        hbox.setMinWidth(600);
        hbox.prefHeightProperty().bind(stage.heightProperty());
        return hbox;
    }

    private VBox createMainWindow() {
        VBox vbox = new VBox();
        vbox.setMinHeight(300);
        vbox.setMinWidth(600);
        return vbox;
    }

    private GridPane createLeftPane() {
        GridPane leftGridPane = new GridPane();
        leftGridPane.add(new Text("Probability"), 0, 0);
        leftGridPane.add(this.probTextField, 1, 0);
        leftGridPane.add(new Text("Amount of Nodes"), 0, 1);
        leftGridPane.add(this.amountOfNodesTextField, 1, 1);
        leftGridPane.add(new Text("Amount Edges"), 0, 2);
        leftGridPane.add(this.amountEdgesTextField, 1, 2);
        leftGridPane.add(new Text("Degree"), 0, 3);
        leftGridPane.add(this.degreeTextField, 1, 3);
        return leftGridPane;
    }

    public static void main(String args[]) {
        launch(args);
    }
}

enum GraphModel {
    GNP,
    PrefentialAttachment,
    GNM;
}